Feature: Billing Rule

  Background: Endpoint Authentication
    Given user is authenticated in the invoicing gateway api

  @regression
  Scenario: Analyst should be able to register a new rule with range type TPV and service type ANALYSIS
    When user send a request with profile "TPV" and service "ANALYSIS"
     And user send a request with "minValue" equals 0.0 and "maxValue" equals 99999999999999999.99999
     And user send a request with "tptFee" equals 0.20 and "tpvFee" equals 0.12
     And user send a request with "paymentMethod" equals "ANY"
     And user makes the "POST" request with path "/invoicing-gateway-api/billingrules"
    Then the status code is 200
     And the response body contains "minValue" equals "0.0"
     And the response body contains "maxValue" equals "99999999999999999.99999"
     And the response body contains "tptFee" with value equals 0.2
     And the response body contains "tpvFee" with value equals 0.12