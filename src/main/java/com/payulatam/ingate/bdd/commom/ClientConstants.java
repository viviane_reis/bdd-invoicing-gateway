/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.commom;

import java.util.Properties;

public class ClientConstants {

	/** The application properties name */
	private static final String APPLICATION_PROPERTIES = "app.properties";

	/** The Constant APPLICATION_JSON. */
	public static final String APPLICATION_JSON = "application/json";

	/** The Constant BASE_URI. */
	public static final String BASE_URI;

	/** The Constant AUTHENTICATION KEY USERNAME. */
	public static final String SECURY_USER_NAME;
	
	/** The Constant AUTHENTICATION KEY PASSWORD. */
	public static final String SECURY_USER_PASSWORD;

	static {

		final Properties properties = PropertyLoader.loadFromClassPath(APPLICATION_PROPERTIES);
		BASE_URI = properties.getProperty("ingate.api.url");
		SECURY_USER_NAME = properties.getProperty("ingate.api.user");
		SECURY_USER_PASSWORD = properties.getProperty("ingate.api.password");
	}

}
