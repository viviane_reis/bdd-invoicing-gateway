/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.commom;

import java.io.IOException;
import java.util.Properties;

public final class PropertyLoader {

	private PropertyLoader() {
		// default empty private constructor
	}

	public static Properties loadFromClassPath(final String propertiesFileName) {

		try {

			final Properties prop = new Properties();
			prop.load(PropertyLoader.class.getClassLoader().getResourceAsStream(propertiesFileName));
			return prop;
		} catch (IOException e) {

			throw new IllegalStateException("Could not load the property file from classpath: " + propertiesFileName);
		}
	}

}
