/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.commom;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

public class BaseUtil {
    public Response RESPONSE;
    public ValidatableResponse JSON;
    public RequestSpecification REQUEST;
    public Integer ID;
    public Integer ID_PROFILE;
    public Integer ID_SERVICE;
    public static final Map<String, Object> PROFILE = new HashMap<>();
    public static final Map<String, Object> SERVICE = new HashMap<>();
    public static final Map<String, Object> RULE = new HashMap<>();
}
