/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.steps;

import com.payulatam.ingate.bdd.commom.BaseUtil;
import static com.payulatam.ingate.bdd.commom.ClientConstants.BASE_URI;
import static com.payulatam.ingate.bdd.commom.ClientConstants.APPLICATION_JSON;
import static com.payulatam.ingate.bdd.commom.ClientConstants.SECURY_USER_NAME;
import static com.payulatam.ingate.bdd.commom.ClientConstants.SECURY_USER_PASSWORD;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.useRelaxedHTTPSValidation;
import static org.hamcrest.Matchers.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;

public class BillingProfileSteps extends BaseUtil {

    private BaseUtil baseUtil;


    public BillingProfileSteps(BaseUtil baseUtil) {
        this.baseUtil = baseUtil;

        baseURI = BASE_URI;
        useRelaxedHTTPSValidation();
    }

    @Given("^user is authenticated in the invoicing gateway api$")
    public void apiAuthentication() {

        baseUtil.REQUEST = given().
                contentType(APPLICATION_JSON).
                auth().
                basic(SECURY_USER_NAME, SECURY_USER_PASSWORD);
    }

    @When("^user send a request with country \"(.*?)\" and rangeType \"(.*?)\"$")
    public void requestProfileData(String country, String rangeType) {

        baseUtil.PROFILE.put("country", country);
        baseUtil.PROFILE.put("rangeType", rangeType);
        baseUtil.REQUEST = baseUtil.REQUEST.
                body(baseUtil.PROFILE);
    }

    @When("^user makes the \"(.*?)\" request with path \"(.*?)\"$")
    public void httpMethods(String method, String pathMeth) {
        switch (pathMeth) {
            case "/invoicing-gateway-api/billingprofiles":
                baseUtil.RESPONSE = baseUtil.REQUEST.
                        when().
                        post(pathMeth);
                baseUtil.ID = baseUtil.RESPONSE.
                        then().
                        extract().
                        path("id");
                break;

            case "/invoicing-gateway-api/billingrules":
                baseUtil.RULE.put("billingServiceId", baseUtil.ID_SERVICE);
                baseUtil.REQUEST = baseUtil.REQUEST.
                        body(baseUtil.RULE);
                baseUtil.RESPONSE = baseUtil.REQUEST.
                        when().
                        post(pathMeth);
                baseUtil.ID = baseUtil.RESPONSE.
                        then().
                        extract().
                        path("id");
                break;
        }
        if (method.equals("GET")) {
            baseUtil.RESPONSE = baseUtil.REQUEST.
                    when().
                    get(pathMeth);
        }
    }


    @Then("^the status code is (\\d+)$")
    public void verifyStatusCode(int statusCode) {
        JSON = baseUtil.RESPONSE.
                then().
                assertThat().
                statusCode(statusCode);
    }

    @Then("^the response body contains \"(.*?)\" with integer value$")
    public void responseParam(String param) {

        baseUtil.RESPONSE.
                then().
                assertThat().
                contentType(ContentType.JSON).
                body(param, equalTo(baseUtil.ID));
    }

    @Then("^the response body contains \"(.*?)\" equals \"(.*?)\"$")
    public void responseBodyEquals(String param, String value) {

        baseUtil.RESPONSE.
                then().
                assertThat().
                contentType(ContentType.JSON).
                body(param, equalTo(value));
    }

    @Then("^the response body contains \"(.*?)\" with value \"(.*?)\"$")
    public void responseBodyContains(String param, String value) {

        baseUtil.RESPONSE.
                then().
                assertThat().
                contentType(ContentType.JSON).
                body(param, hasItems(value));
    }

    @Given("^user makes the GET request to profileId \"(.*?)\"$")
    public void requestGetProfile(String rangeType) {
        baseUtil.PROFILE.put("country", "BR");
        baseUtil.PROFILE.put("rangeType", rangeType);
        baseUtil.RESPONSE = baseUtil.REQUEST.
                body(baseUtil.PROFILE).
                when().
                post("invoicing-gateway-api/billingprofiles");

        baseUtil.ID = baseUtil.RESPONSE.
                then().
                statusCode(200).
                extract().
                path("id");

        baseUtil.RESPONSE = baseUtil.REQUEST.
                when().
                get("invoicing-gateway-api/billingprofiles/{id}", baseUtil.ID);
    }

    @Given("^user makes the POST request with country \"(.*?)\" and profile \"(.*?)\"$")
    public void requestProfile(String country, String rangeType) {
        baseUtil.PROFILE.put("country", country);
        baseUtil.PROFILE.put("rangeType", rangeType);
        baseUtil.RESPONSE = baseUtil.REQUEST.
                body(baseUtil.PROFILE).
                when().
                post("invoicing-gateway-api/billingprofiles");
        baseUtil.ID_PROFILE = baseUtil.RESPONSE.
                then().
                assertThat().
                statusCode(200).
                extract().
                path("id");
    }
}