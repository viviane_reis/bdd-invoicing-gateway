/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.steps;

import com.payulatam.ingate.bdd.commom.BaseUtil;

import cucumber.api.java.en.Given;

public class BillingServiceSteps extends BaseUtil {

    private BaseUtil baseUtil;

    public BillingServiceSteps(BaseUtil baseUtil) {
        this.baseUtil = baseUtil;
    }

    @Given("^user makes the POST request with service \"(.*?)\"$")
    public void requestService(String serviceType) {
        baseUtil.SERVICE.put("billingProfileId",baseUtil.ID_PROFILE);
        baseUtil.SERVICE.put("serviceType", serviceType);
        baseUtil.RESPONSE = baseUtil.REQUEST.
                body(baseUtil.SERVICE).
                when().
                post("invoicing-gateway-api/billingservices");
        baseUtil.ID = baseUtil.RESPONSE.
                then().
                extract().
                path("id");

    }

    @Given("^user makes a GET request serviceType by serviceId$")
    public void requestServiceId() {
        baseUtil.RESPONSE = baseUtil.REQUEST.
                when().
                get("invoicing-gateway-api/billingservices/{id}", baseUtil.ID);
    }

    @Given("^user makes a GET request to list the serviceType by profileId \"(.*?)\"$")
    public void requestProfileServiceType(String rangeType) {
        baseUtil.RESPONSE = baseUtil.REQUEST.
                param("profileId", baseUtil.ID_PROFILE).
                when().
                get("invoicing-gateway-api/billingservices");
    }
}