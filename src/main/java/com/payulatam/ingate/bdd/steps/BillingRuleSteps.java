/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.steps;

import com.payulatam.ingate.bdd.commom.BaseUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;

import static org.hamcrest.Matchers.equalTo;

public class BillingRuleSteps extends BaseUtil {

    private BaseUtil baseUtil;


    public BillingRuleSteps(BaseUtil baseUtil) {
        this.baseUtil = baseUtil;
    }

    @Given("^user send a request with profile \"(.*?)\" and service \"(.*?)\"$")
    public void requestService(String rangeType, String serviceType) {
        baseUtil.PROFILE.put("country", "BR");
        baseUtil.PROFILE.put("rangeType", rangeType);
        baseUtil.RESPONSE = baseUtil.REQUEST.
                body(baseUtil.PROFILE).
                when().
                post("invoicing-gateway-api/billingprofiles");
        baseUtil.ID_PROFILE = baseUtil.RESPONSE.
                then().
                assertThat().
                statusCode(200).
                extract().
                path("id");

        baseUtil.SERVICE.put("billingProfileId", baseUtil.ID_PROFILE);
        baseUtil.SERVICE.put("serviceType", serviceType);
        baseUtil.RESPONSE = baseUtil.REQUEST.
                body(baseUtil.SERVICE).
                when().
                post("invoicing-gateway-api/billingservices");
        baseUtil.ID_SERVICE = baseUtil.RESPONSE.
                then().
                extract().
                path("id");
    }

    @When("^user send a request with \"([^\"]*)\" equals ([+-]?\\d*\\.\\d+)(?![-+0-9\\.]) and \"([^\"]*)\" equals ([+-]?\\d*\\.\\d+)(?![-+0-9\\.])$")
    public void requestRuleRange(String param1, String value1, String param2, String value2) {
        baseUtil.RULE.put(param1, value1);
        baseUtil.RULE.put(param2, value2);
    }

    @When("^user send a request with \"(.*?)\" equals \"(.*?)\"$")
    public void requestRulePymentMethod(String param, String value) {
        baseUtil.RULE.put("billingServiceId", baseUtil.ID_SERVICE);
        baseUtil.RULE.put(param, value);
    }

    @Then("^the response body contains \"(.*?)\" with value equals ([+-]?\\d*\\.\\d+)(?![-+0-9\\.])$")
    public void responseBodyValue(String param, String value) {
        baseUtil.RESPONSE.
                then().
                assertThat().
                contentType(ContentType.JSON).
                body(param, equalTo(value));
    }
}
