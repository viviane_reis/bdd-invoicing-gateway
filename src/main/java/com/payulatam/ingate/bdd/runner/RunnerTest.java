/*
 * PayU Latam - Copyright (c) 2013 - 2018
 * http://www.payulatam.com
 * Date: 22/06/2018
 */

package com.payulatam.ingate.bdd.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.stream.Stream;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
		features = "src/main/resources/features",
		glue = "com.payulatam.ingate.bdd.steps",
		tags = "@regression",
		monochrome = true
		)
public class RunnerTest {
	
	 private static final String FEATURE_PATH = "classpath:features";

	 private static final String FEATURE_FORMAT = FEATURE_PATH + "/%s.feature";
	
	
	 public static void main(String[] args) throws Throwable {
		
		 final Stream<String> features = getFeaturesOrDefault(args);

	     final Stream<String> arguments = Stream.of(
	     		"--glue", "com.payulatam.ingate.bdd.steps",
	     		"--plugin", "pretty",
	     		"--plugin", "html:target/cucumber",
	     		"--plugin", "json:target/cucumber.json"
	     );

	     final String[] parameters = Stream.concat(arguments, features)
	     		.toArray(String[]::new);

	     System.out.println(Arrays.toString(parameters));

	    cucumber.api.cli.Main.main(parameters);
	 }

	 private static Stream<String> getFeaturesOrDefault(final String[] args) {

	 	if (args.length == 0) {

	 		return Stream.of(FEATURE_PATH);
	 	} else {

	 		return Arrays.stream(args)
	 				.map(f -> String.format(FEATURE_FORMAT, f));
	 	}
	 }
}