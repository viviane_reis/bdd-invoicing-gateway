### Automation with Java/Cucumber

## 1 - Install Maven 3
    $ sudo apt-get install maven
    $ mvn -version

-----------------------
## 2 - Install Java 8
    $ sudo add-apt-repository ppa:webupd8team/java
    $ sudo apt-get update
    $ sudo apt-get install oracle-java8-installer
    $ java -version

----------------------- 
## 4 - Clone project  
 $ git clone git@bitbucket.org:pagosonline/invoicing-gateway.git

-----------------------
## 5 - Open project
    $ cd invoicing-gateway/bdd

-----------------------
## 6 - Install dependencies
    $ mvn clean install

-----------------------
## 7 - Run tests

### 7.1 - run all tests
    $ java -jar  target/bdd.jar

### 7.2 - run tests with cucumber options
    $ java -jar  target/bdd.jar BillingService
    $ java -jar  target/bdd.jar BillingProfile

-----------------------