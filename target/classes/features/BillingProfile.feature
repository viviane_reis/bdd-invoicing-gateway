Feature: Billing Profile

  Background: Endpoint Authentication
    Given user is authenticated in the invoicing gateway api

  @regression
  Scenario: Analyst should be able to register a new profile with range type TPV
     When user send a request with country "BR" and rangeType "TPV"
      And user makes the "POST" request with path "/invoicing-gateway-api/billingprofiles"
     Then the status code is 200
      And the response body contains "id" with integer value
      And the response body contains "country" equals "BR"
      And the response body contains "rangeType" equals "TPV"

  @regression
  Scenario: Analyst should be able to register a new profile with range type TPT
     When user send a request with country "BR" and rangeType "TPT"
      And user makes the "POST" request with path "/invoicing-gateway-api/billingprofiles"
     Then the status code is 200
      And the response body contains "id" with integer value
      And the response body contains "country" equals "BR"
      And the response body contains "rangeType" equals "TPT"

  @regression
  Scenario: Analyst should be able to register a new profile with range type TX_VALUE
     When user send a request with country "BR" and rangeType "TX_VALUE"
      And user makes the "POST" request with path "/invoicing-gateway-api/billingprofiles"
     Then the status code is 200
      And the response body contains "id" with integer value
      And the response body contains "country" equals "BR"
      And the response body contains "rangeType" equals "TX_VALUE"

  @regression
  Scenario: Analyst should be able to register a new profile with range type PAYMENT_METHOD
     When user send a request with country "BR" and rangeType "PAYMENT_METHOD"
      And user makes the "POST" request with path "/invoicing-gateway-api/billingprofiles"
     Then the status code is 200
      And the response body contains "id" with integer value
      And the response body contains "country" equals "BR"
      And the response body contains "rangeType" equals "PAYMENT_METHOD"

  @regression
  Scenario: Analyst should be able to search for a profile PAYMENT_METHOD through its id
     When user makes the GET request to profileId "PAYMENT_METHOD"
     Then the status code is 200
      And the response body contains "id" with integer value
      And the response body contains "country" equals "BR"
      And the response body contains "rangeType" equals "PAYMENT_METHOD"

  @regression
  Scenario: Analyst should be able to search for a profile TPV through its id
    When user makes the GET request to profileId "TPV"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "country" equals "BR"
     And the response body contains "rangeType" equals "TPV"

  @regression
  Scenario: Analyst should be able to search for a profile TPT through its id
    When user makes the GET request to profileId "TPT"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "country" equals "BR"
     And the response body contains "rangeType" equals "TPT"

  @regression
  Scenario: Analyst should be able to search for a profile TX_VALUE through its id
    When user makes the GET request to profileId "TX_VALUE"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "country" equals "BR"
     And the response body contains "rangeType" equals "TX_VALUE"

  @regression
  Scenario: Analyst should be see a error message when a GET profile ID nonexistent
     When user makes the "GET" request with path "/invoicing-gateway-api/billingprofiles/0"
     Then the status code is 404
      And the response body contains "message" equals "BillingProfile not found with id 0"