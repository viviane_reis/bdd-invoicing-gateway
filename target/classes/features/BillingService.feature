Feature: Billing Service

  Background: Endpoint Authentication
    Given user is authenticated in the invoicing gateway api

  @regression
  Scenario: Analyst should be able to register a new ANALYSIS service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new PROCESSING service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to register a new PREMIUM_ANALYSIS service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new SIT service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "SIT"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to register a new MANUAL_VALIDATION service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to register a more than ANALYSIS service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PROCESSING service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PROCESSING"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PREMIUM_ANALYSIS service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than SIT service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "SIT"
     And user makes the POST request with service "SIT"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than MANUAL_VALIDATION service for a TPV profile
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a new ANALYSIS service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new PROCESSING service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to register a new PREMIUM_ANALYSIS service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new SIT service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "SIT"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to register a new MANUAL_VALIDATION service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to register a more than ANALYSIS service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PROCESSING service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "PROCESSING"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PREMIUM_ANALYSIS service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than SIT service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "SIT"
     And user makes the POST request with service "SIT"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than MANUAL_VALIDATION service for a TPT profile
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a new ANALYSIS service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new PROCESSING service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to register a new PREMIUM_ANALYSIS service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new SIT service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "SIT"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to register a new MANUAL_VALIDATION service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to register a more than ANALYSIS service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PROCESSING service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "PROCESSING"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PREMIUM_ANALYSIS service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than SIT service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "SIT"
     And user makes the POST request with service "SIT"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than MANUAL_VALIDATION service for a TX_VALUE profile
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a new ANALYSIS service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new PROCESSING service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to register a new PREMIUM_ANALYSIS service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to register a new SIT service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "SIT"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to register a new MANUAL_VALIDATION service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 200
     And the response body contains "id" with integer value
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to register a more than ANALYSIS service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"


  @regression
  Scenario: Analyst should be able to register a more than PROCESSING service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "PROCESSING"
     And user makes the POST request with service "PROCESSING"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than PREMIUM_ANALYSIS service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than SIT service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "SIT"
     And user makes the POST request with service "SIT"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to register a more than MANUAL_VALIDATION service for a PAYMENT_METHOD profile
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes the POST request with service "MANUAL_VALIDATION"
    Then the status code is 400
     And the response body contains "message" equals "Service type already exists"

  @regression
  Scenario: Analyst should be able to search a ANALYSIS service for a TPV profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to search PROCESSING service for a TPV profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PROCESSING"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to search PREMIUM_ANALYSIS service for a TPV profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to search SIT service for a TPV profile its serviceId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "SIT"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to search MANUAL_VALIDATION service for a TPV profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to search ANALYSIS service for a TPT profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to search PROCESSING service for a TPT profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "PROCESSING"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to search PREMIUM_ANALYSIS service for a TPT profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to search SIT service for a TPT profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "SIT"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to search MANUAL_VALIDATION service for a TPT profile through its serviceId
    When user makes the POST request with country "BR" and profile "TPT"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to search ANALYSIS service for a TX_VALUE profile through its serviceId
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to search PROCESSING service for a TX_VALUE profile through its serviceId
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "PROCESSING"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to search PREMIUM_ANALYSIS service for a TX_VALUE profile through its serviceId
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to search SIT service for a TX_VALUE profile through its serviceId
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "SIT"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to search MANUAL_VALIDATION service for a TX_VALUE through its serviceId
    When user makes the POST request with country "BR" and profile "TX_VALUE"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to search ANALYSIS service for a PAYMENT_METHOD profile through its serviceId
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "ANALYSIS"

  @regression
  Scenario: Analyst should be able to search PROCESSING service for a PAYMENT_METHOD profile through its serviceId
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "PROCESSING"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PROCESSING"

  @regression
  Scenario: Analyst should be able to search PREMIUM_ANALYSIS service for a PAYMENT_METHOD profile through its serviceId
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to search SIT service for a PAYMENT_METHOD profile through its serviceId
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "SIT"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "SIT"

  @regression
  Scenario: Analyst should be able to search MANUAL_VALIDATION service for a PAYMENT_METHOD profile through its serviceId
    When user makes the POST request with country "BR" and profile "PAYMENT_METHOD"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes a GET request serviceType by serviceId
    Then the status code is 200
     And the response body contains "serviceType" equals "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be see a error message when a GET service ID nonexistent
    When user makes the "GET" request with path "/invoicing-gateway-api/billingservices/0"
    Then the status code is 404
    And the response body contains "message" equals "Billing Service with id=0 not found."

  @regression
  Scenario: Analyst should be able to list ANALYSIS serviceType for a TPV profile through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "ANALYSIS"

  @regression
  Scenario: Analyst should be able to list PROCESSING serviceType for a TPV profile through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PROCESSING"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "PROCESSING"

  @regression
  Scenario: Analyst should be able to list PREMIUM_ANALYSIS serviceType for a TPV profile through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to list SIT serviceType for a TPV profile through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "SIT"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "SIT"

  @regression
  Scenario: Analyst should be able to list MANUAL_VALIDATION serviceType for a TPV profile through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "PROCESSING"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "ANALYSIS"
     And the response body contains "serviceType" with value "PROCESSING"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "ANALYSIS"
     And the response body contains "serviceType" with value "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "SIT"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "ANALYSIS"
     And the response body contains "serviceType" with value "SIT"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "MANUAL_VALIDATION"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "ANALYSIS"
     And the response body contains "serviceType" with value "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
    When user makes the POST request with country "BR" and profile "TPV"
     And user makes the POST request with service "ANALYSIS"
     And user makes the POST request with service "PROCESSING"
     And user makes the POST request with service "PREMIUM_ANALYSIS"
     And user makes a GET request to list the serviceType by profileId "TPV"
    Then the status code is 200
     And the response body contains "serviceType" with value "ANALYSIS"
     And the response body contains "serviceType" with value "PROCESSING"
     And the response body contains "serviceType" with value "PREMIUM_ANALYSIS"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
     When user makes the POST request with country "BR" and profile "TPV"
      And user makes the POST request with service "ANALYSIS"
      And user makes the POST request with service "PROCESSING"
      And user makes the POST request with service "SIT"
      And user makes a GET request to list the serviceType by profileId "TPV"
     Then the status code is 200
      And the response body contains "serviceType" with value "ANALYSIS"
      And the response body contains "serviceType" with value "PROCESSING"
      And the response body contains "serviceType" with value "SIT"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
     When user makes the POST request with country "BR" and profile "TPV"
      And user makes the POST request with service "ANALYSIS"
      And user makes the POST request with service "PROCESSING"
      And user makes the POST request with service "MANUAL_VALIDATION"
      And user makes a GET request to list the serviceType by profileId "TPV"
     Then the status code is 200
      And the response body contains "serviceType" with value "ANALYSIS"
      And the response body contains "serviceType" with value "PROCESSING"
      And the response body contains "serviceType" with value "MANUAL_VALIDATION"

  @regression
  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
     When user makes the POST request with country "BR" and profile "TPV"
      And user makes the POST request with service "ANALYSIS"
      And user makes the POST request with service "PREMIUM_ANALYSIS"
      And user makes the POST request with service "SIT"
      And user makes a GET request to list the serviceType by profileId "TPV"
     Then the status code is 200
      And the response body contains "serviceType" with value "ANALYSIS"
      And the response body contains "serviceType" with value "PREMIUM_ANALYSIS"
      And the response body contains "serviceType" with value "SIT"

  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
     When user makes the POST request with country "BR" and profile "TPV"
      And user makes the POST request with service "ANALYSIS"
      And user makes the POST request with service "PREMIUM_ANALYSIS"
      And user makes the POST request with service "MANUAL_VALIDATION"
      And user makes a GET request to list the serviceType by profileId "TPV"
     Then the status code is 200
      And the response body contains "serviceType" with value "ANALYSIS"
      And the response body contains "serviceType" with value "PREMIUM_ANALYSIS"
      And the response body contains "serviceType" with value "MANUAL_VALIDATION"

  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
     When user makes the POST request with country "BR" and profile "TPV"
      And user makes the POST request with service "ANALYSIS"
      And user makes the POST request with service "SIT"
      And user makes the POST request with service "MANUAL_VALIDATION"
      And user makes a GET request to list the serviceType by profileId "TPV"
     Then the status code is 200
      And the response body contains "serviceType" with value "ANALYSIS"
      And the response body contains "serviceType" with value "SIT"
      And the response body contains "serviceType" with value "MANUAL_VALIDATION"

  Scenario: Analyst should be able to list serviceType for a profile TPV through its profileId
     When user makes the POST request with country "BR" and profile "TPV"
      And user makes the POST request with service "ANALYSIS"
      And user makes the POST request with service "PROCESSING"
      And user makes the POST request with service "PREMIUM_ANALYSIS"
      And user makes the POST request with service "SIT"
      And user makes a GET request to list the serviceType by profileId "TPV"
     Then the status code is 200
      And the response body contains "serviceType" with value "ANALYSIS"
      And the response body contains "serviceType" with value "PROCESSING"
      And the response body contains "serviceType" with value "PREMIUM_ANALYSIS"
      And the response body contains "serviceType" with value "SIT"